import React from "react";
import { useSelector } from "react-redux";
import {
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";

function Users() {
  const users = useSelector((state) => state.data.users);
  
  const formatDate = (data) => {
    const date = new Date(data);
    const month = date.getUTCMonth() + 1;
    const day = date.getUTCDate();
    const year = date.getUTCFullYear();

    return `${day}/${month}/${year}`
  }
  
  return (
    <CTable align="middle" className="mb-0 border" hover responsive>
      <CTableHead color="light">
        <CTableRow>
          {/* <CTableHeaderCell className="text-center">
          <CIcon icon={cilPeople} />
        </CTableHeaderCell> */}
          <CTableHeaderCell >User</CTableHeaderCell>
          <CTableHeaderCell className="text-center">Email</CTableHeaderCell>
          <CTableHeaderCell className="text-center">Telephone</CTableHeaderCell>
          <CTableHeaderCell className="text-center">Last activity</CTableHeaderCell>
          <CTableHeaderCell className="text-center">Action</CTableHeaderCell>
        </CTableRow>
      </CTableHead>
      <CTableBody>
        {users.map((item) => (
          <CTableRow v-for="item in tableItems" key={item.id}>
            {/* <CTableDataCell className="text-center">
            <CAvatar size="md" src={item.avatar.src} status={item.avatar.status} />
          </CTableDataCell> */}
            <CTableDataCell>
              <div>{item.name}</div>
              <div className="small text-medium-emphasis">
                <span>id: {item.id}</span>
                {/* {item.user.registered} */}
              </div>
            </CTableDataCell>
            {/* <CTableDataCell className="text-center">
            <CIcon size="xl" icon={item.country.flag} title={item.country.name} />
          </CTableDataCell> */}
            {/* <CTableDataCell>
            <div className="clearfix">
              <div className="float-start">
                <strong>{item.usage.value}%</strong>
              </div>
              <div className="float-end">
                <small className="text-medium-emphasis">{item.usage.period}</small>
              </div>
            </div>
            <CProgress thin color={item.usage.color} value={item.usage.value} />
          </CTableDataCell> */}
            <CTableDataCell className="text-center">
              <div className="small text-medium-emphasis">
                <span>{item.user.email}</span>
              </div>
            </CTableDataCell>
            <CTableDataCell className="text-center">
              <div className="small text-medium-emphasis">
                <span>{item.telephone}</span>
              </div>
            </CTableDataCell>
            <CTableDataCell className="text-center">
              <div className="small text-medium-emphasis">
                <span>{formatDate(item.user.lastLoginAt)}</span>
              </div>
            </CTableDataCell>
            <CTableDataCell className="text-center">
            <button class="btn btn-danger" type="button">Delete</button>
            </CTableDataCell>
          </CTableRow>
        ))}
      </CTableBody>
    </CTable>
  );
}

export default Users;
