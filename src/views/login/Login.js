import React, { useState } from "react";
import { signIn } from "src/services/api";

function Login() {
  const [credentials, setCredentials] = useState({});

  const handleChange = ({ target }) => {
    const { name, value } = target;

    setCredentials((state) => ({ ...state, [name]: value }));

    console.log(credentials);
  };

  const handleAuth = async (event) => {
    event.preventDefault();
    const request = await signIn(credentials);

    console.log(request);
  };
  return (
    <form onSubmit={(event) => handleAuth(event)}>
      <div class="form-group">
        <label for="inputAddress">Email</label>
        <input
          name="email"
          type="text"
          class="form-control"
          id="inputAddress"
          placeholder="example@test.com"
          onChange={(event) => handleChange(event)}
        />
      </div>
      <br />
      <div class="form-group">
        <label for="inputPassword2">Password</label>
        <input
          name="password"
          type="password"
          class="form-control"
          placeholder="***********"
          id="inputPassword2"
          onChange={(event) => handleChange(event)}
        />
      </div>
      <br />
      <div className="text-center ">
        <button type="submit" className="btn btn-primary">
          Sign in
        </button>
      </div>
    </form>
  );
}

export default Login;
