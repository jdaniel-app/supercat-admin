import React, { lazy, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import { CProgress, CProgressBar } from "@coreui/react";

import {
  getAdverts,
  getReports,
  getStores,
  getUsers,
} from "src/services/api.js";

const WidgetsDropdown = lazy(() => import("../widgets/WidgetsDropdown.js"));

const Dashboard = () => {
  const [adminData, setAdminData] = useState([]);
  const [loadedElements, setLoadedElements] = useState("0");
  const dispatch = useDispatch();
  const random = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };
  const appData = useSelector((state) => state.data);

  const loadedResponses = {
    0: "Fetching data...",
    25: "Setting users",
    50: "Recovering adverts",
    75: "We are almost there, be pacient...",
    100: "Done!",
  };

  const requestData = async () => {
    const adverts = await getAdverts();
    setLoadedElements("25");
    const reports = await getReports();
    setLoadedElements("50");
    const users = await getUsers();
    setLoadedElements("75");
    const stores = await getStores();
    setLoadedElements("100");
    setAdminData({
      stores: stores.data,
      users: users.data,
      adverts: adverts.data.data,
      reports: reports.data,
    });
  };

  useEffect(async () => {
    if (!appData.adverts) {
      await requestData();
    }
  }, []);

  useEffect(() => {
    if (!appData.adverts) {
      dispatch({ type: "save_data", data: adminData });
    }
  }, [adminData]);

  return (
    <>
      {loadedElements === "100" || appData.adverts ? (
        <WidgetsDropdown />
      ) : (
        <>
          <strong>{loadedResponses[loadedElements]}</strong>
          <CProgress class="mb-3">
            <CProgressBar
              color="info"
              variant="striped"
              animated
              value={loadedElements}
            />
          </CProgress>
        </>
      )}
    </>
  );
};

export default Dashboard;
