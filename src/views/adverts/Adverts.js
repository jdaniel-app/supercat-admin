import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CButton,
  CFormInput,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";
import { deleteAdvert, updateAdvert } from "src/services/api";

function Adverts() {
  const adverts = useSelector((state) => state.data.adverts);
  const deletedElements = useSelector((state) => state.deleted);
  const [selectedAdvert, setSelectedAdvert] = useState({});
  const [visible, setVisible] = useState(false);
  const [isFetching, setIsFetching] = useState(false);
  const dispatch = useDispatch();

  const handleAdvert = (advert) => {
    setSelectedAdvert({
      id: advert.id,
      price: advert.price,
      plate: advert.plate,
      km: advert.km,
      year: advert.year,
      fabYear: advert.fabYear,
      modelId: advert.modelId,
      metadataOptions: advert.metadataOptions,
      pictures: advert.pictures,
      user: advert.userId,
      userId: advert.userId,
      step: advert.step,
    });
    setVisible(true);
  };

  const formatDate = (data) => {
    const date = new Date(data);
    const month = date.getUTCMonth() + 1;
    const day = date.getUTCDate();
    const year = date.getUTCFullYear();

    return `${day}/${month}/${year}`;
  };

  const handleChange = ({ target }) => {
    const { name, value } = target;
    setSelectedAdvert((state) => ({
      ...state,
      [name]: name === "plate" ? value : Number(value),
    }));
  };

  const handleUpdate = async () => {
    console.log("request");
    setIsFetching(true);
    const obj = await updateAdvert(selectedAdvert.id, selectedAdvert);
    dispatch({ type: "update_advert", data: obj });
    setIsFetching(false);
    setVisible(false);
  };

  const handleDelete = async (id) => {
    console.log("delete");
    const request = await deleteAdvert(id);
    console.log(request);
    dispatch({ type: "deleted_advert", id });
  };

  const turnOffRow = (id) => {
    return deletedElements.find((element) => element === id);
  };

  return (
    <>
      <CTable align="middle" className="mb-0 border" hover responsive>
        <CTableHead color="light">
          <CTableRow className="text-center">
            <CTableHeaderCell>Advert Info</CTableHeaderCell>
            <CTableHeaderCell>Version</CTableHeaderCell>
            <CTableHeaderCell>Brand</CTableHeaderCell>
            <CTableHeaderCell>Year</CTableHeaderCell>
            <CTableHeaderCell>Price</CTableHeaderCell>
            <CTableHeaderCell>CreatedAt</CTableHeaderCell>
            <CTableHeaderCell>Actions</CTableHeaderCell>
            <CTableHeaderCell></CTableHeaderCell>
          </CTableRow>
        </CTableHead>
        <CTableBody>
          {adverts.map((item) => (
            <CTableRow
              v-for="item in tableItems"
              key={item.id}
              className={`${
                turnOffRow(item.id) ? "table-dark" : ""
              } "text-center"`}
            >
              <CTableDataCell>
                <div>{item.user.email}</div>
                <div className="small text-medium-emphasis">
                  <span>Advert id: {item.id}</span>/
                  <span>User id: {item.userId}</span>
                </div>
              </CTableDataCell>
              <CTableDataCell>
                <div>{item.model.version}</div>
                <div className="small text-medium-emphasis">
                  <span>type: {item.fuelType}</span>/
                  <span>gear: {item.gearType}</span>/
                  <span>color: {item.color}</span>
                </div>
              </CTableDataCell>
              <CTableDataCell>
                <div className="small text-medium-emphasis">
                  <span>{item.model.brand.name}</span>
                </div>
              </CTableDataCell>
              <CTableDataCell>
                <div className="small text-medium-emphasis">
                  <span>{item.year}</span>
                </div>
              </CTableDataCell>
              <CTableDataCell>
                <div className="small text-medium-emphasis">
                  <span>{item.price} R$</span>
                </div>
              </CTableDataCell>
              <CTableDataCell>
                <div className="small text-medium-emphasis">
                  <span>{formatDate(item.createdAt)}</span>
                </div>
              </CTableDataCell>
              <CTableDataCell>
                <button
                  className={`btn ${
                    turnOffRow(item.id) ? "btn-dark" : "btn-success"
                  }`}
                  type="button"
                  onClick={() => handleAdvert(item)}
                  disabled={turnOffRow(item.id)}
                >
                  Update
                </button>
              </CTableDataCell>
              <CTableDataCell>
                <button
                  className={`btn ${
                    turnOffRow(item.id) ? "btn-dark" : "btn-danger"
                  }`}
                  type="button"
                  onClick={() => handleDelete(item.id)}
                  disabled={turnOffRow(item.id)}
                >
                  Delete
                </button>
              </CTableDataCell>
            </CTableRow>
          ))}
        </CTableBody>
      </CTable>
      <CModal visible={visible} onClose={() => setVisible(false)}>
        <CModalHeader>
          <CModalTitle>Update</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <span>id</span>
          <CFormInput
            type="text"
            placeholder="Default input"
            aria-label="default input example"
            value={selectedAdvert && selectedAdvert.id}
            disabled
            onChange={(event) => handleChange(event)}
          />
          <span>Price</span>
          <CFormInput
            type="text"
            name="price"
            placeholder="Default input"
            aria-label="default input example"
            value={selectedAdvert && selectedAdvert.price}
            onChange={(event) => handleChange(event)}
          />
          <span>Kilometers</span>
          <CFormInput
            type="text"
            name="km"
            placeholder="Default input"
            aria-label="default input example"
            value={selectedAdvert && selectedAdvert.km}
            onChange={(event) => handleChange(event)}
          />
          <span>Year</span>
          <CFormInput
            type="text"
            name="year"
            placeholder="Default input"
            aria-label="default input example"
            value={selectedAdvert && selectedAdvert.year}
            onChange={(event) => handleChange(event)}
          />
          <span>fabYear</span>
          <CFormInput
            type="text"
            name="fabYear"
            placeholder="Default input"
            aria-label="default input example"
            value={selectedAdvert && selectedAdvert.fabYear}
            onChange={(event) => handleChange(event)}
          />
          <span>Model Id</span>
          <CFormInput
            type="text"
            name="modelId"
            placeholder="Default input"
            aria-label="default input example"
            value={selectedAdvert && selectedAdvert.modelId}
            onChange={(event) => handleChange(event)}
          />
        </CModalBody>
        <CModalFooter>
          <CButton color="secondary" onClick={() => setVisible(false)}>
            Close
          </CButton>
          <CButton color="primary" onClick={() => handleUpdate()}>
            {isFetching ? (
              <div>
                <span
                  className="spinner-border spinner-border-sm"
                  role="status"
                  aria-hidden="true"
                ></span>{" "}
                Updating...
              </div>
            ) : (
              "Update"
            )}
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  );
}

export default Adverts;
