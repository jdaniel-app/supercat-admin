import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

const initialState = {
  sidebarShow: false,
  data: {},
  deleted: [],
};

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case "set":
      return { ...state, ...rest };

    case "deleted_advert":
      return { ...state, deleted: [...state.deleted, rest.id] };

    case "save_data":
      return { ...state, ...rest };

    case "update_advert":
      const index = state.data.adverts.findIndex(
        (element) => element.id === rest.data.id
      );
      return {
        ...state,
        data: {
          ...state.data,
          adverts: [
            ...state.data.adverts.slice(0, index),
            rest.data,
            ...state.data.adverts.slice(index + 1),
          ],
        },
      };

    default:
      return state;
  }
};

const store = createStore(changeState, composeWithDevTools());
export default store;
