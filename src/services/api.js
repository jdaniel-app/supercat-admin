import axios from "axios";

export const signIn = async (data) => {
  return await axios({
    url: "http://localhost:3333/sessions",
    method: "post",
    data,
  }).catch((err) => err.response);
};

export const getAdverts = async () => {
  return await axios({
    url: "http://localhost:3333/adverts",
    method: "get",
  }).catch((err) => err.response);
};

export const updateAdvert = async (id, data) => {
  const request = await axios({
    url: `http://localhost:3333/adverts/${id}`,
    method: "put",
    data: data,
    headers: {
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM4NDMxOTk4LCJleHAiOjE2MzkwMzY3OTh9.3UMPE5TbI-Eh2Omg-ru0vyko6aEVN4B7BB0hQnzx2cY",
    },
  }).catch((err) => err.response);

  console.log(request);

  return request.data;
};

export const deleteAdvert = async (id) => {
  return await axios({
    url: "http://localhost:3333/adverts",
    method: "delete",
    data: {
      id,
    },
    headers: {
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM4NDMxOTk4LCJleHAiOjE2MzkwMzY3OTh9.3UMPE5TbI-Eh2Omg-ru0vyko6aEVN4B7BB0hQnzx2cY",
    },
  }).catch((err) => err.response);
};

export const getReports = async () => {
  return await axios({
    url: "http://localhost:3333/reports",
    method: "get",
  }).catch((err) => err.response);
};

export const getUsers = async () => {
  return await axios({
    url: "http://localhost:3333/clients",
    method: "get",
    headers: {
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM4NDMxOTk4LCJleHAiOjE2MzkwMzY3OTh9.3UMPE5TbI-Eh2Omg-ru0vyko6aEVN4B7BB0hQnzx2cY",
    },
  });
};

export const getStores = async () => {
  return await axios({
    url: "http://localhost:3333/stores",
    method: "get",
    headers: {
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM4NDMxOTk4LCJleHAiOjE2MzkwMzY3OTh9.3UMPE5TbI-Eh2Omg-ru0vyko6aEVN4B7BB0hQnzx2cY",
    },
  });
};

export const deleteUser = async () => {
  return await axios({
    url: "http://localhost:3333/users",
    method: "get",
    headers: {
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjM4NDMxOTk4LCJleHAiOjE2MzkwMzY3OTh9.3UMPE5TbI-Eh2Omg-ru0vyko6aEVN4B7BB0hQnzx2cY",
    },
  });
};
